# Reign Challenge - Frontend Project

  <p align="center">Frontend Application made entirely with React JS</p>

### To have the project functional, the following steps must be followed.

## Presettings

- You must have the [Backend Application](https://gitlab.com/jordansalazar.975/backend-challenge) already running.

## Installation

```bash
$ npm install
```

## Running the app

```bash
$ npm start
```

## Considerations

- The application is not responsive.

## Stay in touch

- Author - [Jordan Salazar](https://github.com/rafalazar)
